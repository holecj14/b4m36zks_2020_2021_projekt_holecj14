/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

/**
 *
 * @author jahol
 */
public class DrawTableAddPlayerTest {
    
    DrawTable dt;
    
    @BeforeEach
    public void initDraw(){
        dt = new DrawTable();
    }
    /**
     * Test of addPlayer method, of class DrawTable.
     * @param playersNames
     */
    @ParameterizedTest
    @ArgumentsSource(DrawTableAddPlayerParametrizedTest.class)
    public void testAddPlayer_playersNum(String[] playersNames) {
        
        for (String playersName : playersNames) {
            dt.addPlayer(playersName);            
        }
       
        assertEquals(playersNames.length, dt.getPlayers().size());
    }
    
    @ParameterizedTest
    @ArgumentsSource(DrawTableAddPlayerParametrizedTest.class)
    public void testAddPlayer_matchesNum(String[] playersNames) {
        
        for (String playersName : playersNames) {
            dt.addPlayer(playersName);            
        }
       
        assertEquals(playersNames.length, dt.matchesResults.size());


    }
    
    @ParameterizedTest
    @ArgumentsSource(DrawTableAddPlayerParametrizedTest.class)
    public void testAddPlayer_matchesNumInside(String[] playersNames) {
        
        for (String playersName : playersNames) {
            dt.addPlayer(playersName);            
        }
       
         for (int i = 0; i < playersNames.length; i++) {
           assertEquals(playersNames.length, dt.matchesResults.get(i).size());
       }
    }
}
