/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Core;

import java.util.Arrays;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;


/**
 *
 * @author jahol
 */
public class DrawTableAddPlayerParametrizedTest implements ArgumentsProvider {
    
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext ec) throws Exception {   
        Object arr1 = (Object) new String[]{"A"};
        Object arr2 = (Object) new String[]{"A", "B"};
        Object arr3 = (Object) new String[]{"A", "B", "C"};
        Object[] arrays = new Object[] {arr1, arr2 , arr3};
        return Stream.of( arrays ).map(Arguments::of);
    }
}
