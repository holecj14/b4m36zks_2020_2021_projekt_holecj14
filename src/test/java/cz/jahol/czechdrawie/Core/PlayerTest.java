/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author jahol
 */
public class PlayerTest {
    
    public PlayerTest() {
    }

    /**
     * Test of getName method, of class Player.
     */
    @Test
    public void testGetEmptyName() {
        Player instance = new Player("");
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetName() {
        Player instance = new Player("A");
        String expResult = "A";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class Player.
     */
    @Test
    public void testSetName() {
        String name = "A";
        Player instance = new Player("");
        instance.setName(name);
        assertEquals(name, instance.getName());
    }

    /**
     * Test of hashCode method, of class Player.
     */
    @Test
    public void testHashEqualCode() {
        Player instance = new Player("A");
        Player instance2 = new Player("A");
        assertEquals(instance.hashCode(), instance2.hashCode());
    }
    
    @Test
    public void testHashDifferCode() {
        Player instance = new Player("A");
        Player instance2 = new Player("B");
        assertNotEquals(instance.hashCode(), instance2.hashCode());
    }

    /**
     * Test of equals method, of class Player.
     */
    @Test
    public void testEqualsTrue() {
        Player instance = new Player("A");
        Player instance2 = new Player("A");
        assertEquals(instance, instance2);    
    }
    
    @Test
    public void testEqualsFalse() {
        Player instance = new Player("A");
        Player instance2 = new Player("B");
        assertNotEquals(instance, instance2);    
    }
    
}
