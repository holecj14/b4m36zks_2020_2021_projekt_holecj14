/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Core;

import cz.jahol.czechdrawie.Exceptions.NoSetResultException;
import cz.jahol.czechdrawie.Exceptions.PlayerNotExistException;
import cz.jahol.czechdrawie.Exceptions.SamePlayerResultException;
import cz.jahol.czechdrawie.Exceptions.SetNotAddYetException;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;

/**
 *
 * @author jahol
 */
public class DrawTableTest {
    
    DrawTable dt;
    
    public DrawTableTest() {
    }
    
    
    @BeforeEach
    public void initDraw(){
        dt = new DrawTable();
    }

    

    @Test
    public void testRemovePlayer_NoExist() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");     
        assertThrows(PlayerNotExistException.class, () -> {dt.removePlayer("C"); });
       
    }
    
    @Test
    public void testRemovePlayerCount() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");
        dt.removePlayer("A");        
        assertEquals(1, dt.getPlayers().size());
    }

    /**
     * Test of getPlayerIndex method, of class DrawTable.
     */
    @Test
    public void testGetPlayerIndex() {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        assertEquals(0, dt.getPlayerIndex("A"));
        assertEquals(1, dt.getPlayerIndex("B"));
    }

    /**
     * Test of addSetResult method, of class DrawTable.
     * @throws java.lang.Exception
     */
        @Test
    public void testAddSetResult_Sameplayer() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");
        assertThrows(SamePlayerResultException.class, () -> {dt.addSetResult("A", "A", 10, 0); });
    }
    
    @Test
    public void testAddSetResult() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");
        dt.addSetResult("A", "B", 10, 20);
        assertEquals(10, dt.matchesResults.get(0).get(1).getSetScore(0)[0]);
        assertEquals(20, dt.matchesResults.get(1).get(0).getSetScore(0)[0]);
    }

    /**
     * Test of changeSetResult method, of class DrawTable.
     */
    
    

    /**
     * Test of changeSetResult method, of class DrawTable.
     * @throws java.lang.Exception
     */
    @Test
    public void testChangeSetResult_NoAdded() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");
        assertThrows(SetNotAddYetException.class, () -> {dt.changeSetResult("A", "B", 0, 0, 1);});
    }
    
    @Test
    public void testChangeSetResult_SamePlayer() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        assertThrows(SamePlayerResultException.class, () -> {dt.changeSetResult("A", "A", 0, 0, 0);});
    }
    
    @Test
    public void testChangeSetResult() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.changeSetResult("A", "B", 15, 25, 0);
        assertEquals(15, dt.matchesResults.get(0).get(1).getSetScore(0)[0]);        
        assertEquals(25, dt.matchesResults.get(1).get(0).getSetScore(0)[0]);
    }

    /**
     * Test of computePlayerResultPointsGive method, of class DrawTable.
     * @throws java.lang.Exception
     */
    @Test
    public void testComputePlayerResultPointsGive() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.addSetResult("A", "B", 15, 25);
        assertEquals(25, dt.computePlayerResultPointsGive("A"));        
        assertEquals(45, dt.computePlayerResultPointsGive("B")); 
    }

    /**
     * Test of computePlayerResultPointsGot method, of class DrawTable.
     * @throws java.lang.Exception
     */
    @Test
    public void testComputePlayerResultPointsGot() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.addSetResult("A", "B", 15, 25);
        assertEquals(25, dt.computePlayerResultPointsGot("B"));        
        assertEquals(45, dt.computePlayerResultPointsGot("A")); 
    }

    /**
     * Test of computePlayerResultPointsDiff method, of class DrawTable.
     * @throws java.lang.Exception
     */
    @Test
    public void testComputePlayerResultPointsDiff() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.addSetResult("A", "B", 15, 25);
        assertEquals(-20, dt.computePlayerResultPointsDiff("A"));        
        assertEquals(20, dt.computePlayerResultPointsDiff("B")); 
    }

    /**
     * Test of computePlayerResultSetsGive method, of class DrawTable.
     * @throws java.lang.Exception
     */
    @Test
    public void testComputePlayerResultSetsGive() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.addSetResult("A", "B", 15, 25);
        assertEquals(0, dt.computePlayerResultSetsGive("A"));        
        assertEquals(2, dt.computePlayerResultSetsGive("B")); 
    }

    /**
     * Test of computePlayerResultSetsGot method, of class DrawTable.
     * @throws java.lang.Exception
     */
    @Test
    public void testComputePlayerResultSetsGot() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.addSetResult("A", "B", 15, 25);
        assertEquals(0, dt.computePlayerResultSetsGot("B"));        
        assertEquals(2, dt.computePlayerResultSetsGot("A")); 
    }

    /**
     * Test of computePlayerResultSetsDiff method, of class DrawTable.
     * @throws java.lang.Exception
     */
    @Test
    public void testComputePlayerResultSetsDiff() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.addSetResult("A", "B", 15, 25);
        assertEquals(-2, dt.computePlayerResultSetsDiff("A"));        
        assertEquals(2, dt.computePlayerResultSetsDiff("B")); 
    }
    
    /**
     * Test of computePlayerResultMatchesGive method, of class DrawTable.
     * @throws java.lang.Exception
     */
    @Test
    public void testComputePlayerResultMatchGive() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.addSetResult("A", "B", 15, 25);
        assertEquals(0, dt.computePlayerResultMatchesGive("A"));        
        assertEquals(1, dt.computePlayerResultMatchesGive("B")); 
    }

    /**
     * Test of computePlayerResultMatchesGot method, of class DrawTable.
     * @throws java.lang.Exception
     */
    @Test
    public void testComputePlayerResultMatchesGot() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.addSetResult("A", "B", 15, 25);
        assertEquals(0, dt.computePlayerResultMatchesGot("B"));        
        assertEquals(1, dt.computePlayerResultMatchesGot("A")); 
    }

    /**
     * Test of computePlayerResultMatchesDiff method, of class DrawTable.
     * @throws java.lang.Exception
     */
    @Test
    public void testComputePlayerResultMatchesDiff() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.addSetResult("A", "B", 15, 25);
        assertEquals(-1, dt.computePlayerResultMatchesDiff("A"));        
        assertEquals(1, dt.computePlayerResultMatchesDiff("B")); 
    }
    /**
     * Test of getPlayersOrder_MatchesSetsPointsDiff method, of class DrawTable.
     */
    @Test
    public void testGetPlayersOrder_MatchesSetsPointsDiff() throws Exception {
        dt.addPlayer("A");
        dt.addPlayer("B");        
        dt.addSetResult("A", "B", 10, 20);
        dt.addSetResult("A", "B", 15, 25);
        ArrayList<Integer> exp = new ArrayList<>();
        exp.add(1); exp.add(0);
        assertEquals(exp.size(), dt.getPlayersOrder_MatchesSetsPointsDiff().size());
        assertEquals( exp, dt.getPlayersOrder_MatchesSetsPointsDiff());
    
    }

    /**
     * Test of getName method, of class DrawTable.
     */
    @Test
    public void testGetSetName() {
        String name = "Test";
        dt.setName(name);
        assertEquals( name ,dt.getName());
    }

    /**
     * Test of getPlayers method, of class DrawTable.
     */
    @Test
    public void testGetPlayers() {
        
        ArrayList<Player> exp = new ArrayList<>();
        exp.add(new Player("A"));
        exp.add(new Player("B"));
        dt.addPlayer("A");
        dt.addPlayer("B");
        assertEquals(exp, dt.getPlayers());
    }
    
}
