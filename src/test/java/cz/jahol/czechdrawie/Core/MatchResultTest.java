/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Core;

import cz.jahol.czechdrawie.Exceptions.NoSetResultException;
import cz.jahol.czechdrawie.Exceptions.SamePlayerResultException;
import cz.jahol.czechdrawie.Exceptions.SetNotAddYetException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 * @author jahol
 */
public class MatchResultTest {
    
    Player p1, p2, p3;
    MatchResult mr;
    
    public MatchResultTest() {
        p1 = new Player("A");
        p2 = new Player("B");
        p3 = new Player("C");
    }

    @BeforeEach
    public void initMatchResultTests(){
        mr = new MatchResult(p1, p2);
    }
    
    /**
     * Test of getPlayer1 method, of class MatchResult.
     */
    @Test
    public void testGetPlayer1() {
        assertEquals(mr.getPlayer1(), p1);
    }
    
    @Test
    public void testGetPlayer1second() {
        assertNotEquals(mr.getPlayer1(), p2);
    }
    
    @Test
    public void testGetPlayer1diff() {
        assertNotEquals(mr.getPlayer1(), p3);
    }
    
    /**
     * Test of getPlayer2 method, of class MatchResult.
     */
    @Test
    public void testGetPlayer2() {
        assertEquals(mr.getPlayer2(), p2);
    }
    
    @Test
    public void testGetPlayer2second() {
        if (mr.getPlayer2() == null){
            System.out.println("Null player 2 in mr");
        }
        assertNotEquals(mr.getPlayer2(), p1);
    }
    
    @Test
    public void testGetPlayer2diff() {
        assertNotEquals(mr.getPlayer2(), p3);
    }
    
    /**
     * Test of addSetScore method, of class MatchResult.
     * @throws java.lang.Exception
     */
    
    @Test
    public void testAddSetScoreSamePlayer() throws Exception {
        int s1 = 10, s2 = 20;
        mr.player2 = p1;
        assertThrows(SamePlayerResultException.class, ()->{mr.addSetScore(s1, s2);});
    }
    
    
    @Test
    public void testAddSetScoreSize() throws Exception {
        int s1 = 10, s2 = 20;
        mr.addSetScore(s1, s2);
        assertEquals(mr.score1.size(), mr.score2.size());
    }
    
    @Test
    public void testAddSetScoreValue1() throws Exception {
        int s1 = 10, s2 = 20;
        mr.addSetScore(s1, s2);
        assertEquals(mr.score1.get(0), s1);
    }
    
    @Test
    public void testAddSetScoreValue2() throws Exception {
        int s1 = 10, s2 = 20;
        mr.addSetScore(s1, s2);
        assertEquals(mr.score2.get(0), s2);
    }
    
    
    /**
     * Test of changeSetScore method, of class MatchResult.
     * @throws java.lang.Exception
     */
    @Test
    public void testChangeSetSamePlayer() throws Exception {
        int s1 = 10, s2 = 20;
        mr.addSetScore(s1, s2);
        mr.player2 = p1;
        assertThrows(SamePlayerResultException.class, ()->{mr.changeSetScore(0, s1, s2);});
    }
    
    @Test
    public void testChangeSetNotAdded() throws Exception {
        int s1 = 10, s2 = 20;
        assertThrows(SetNotAddYetException.class, () -> {mr.changeSetScore(0, s1, s2);});
    }
    
    @Test
    public void testChangeSetValue1() throws Exception{
        int s1 = 10, s2 = 20, s1n = 11, s2n = 21;        
        mr.addSetScore(s1, s2);
        mr.changeSetScore(0, s1n, s2n);
        assertEquals(mr.score1.get(0), s1n);
    }
    
    @Test
    public void testChangeSetValue2() throws Exception {
        int s1 = 10, s2 = 20, s1n = 11, s2n = 21;        
        mr.addSetScore(s1, s2);
        mr.changeSetScore(0, s1n, s2n);
        assertEquals(mr.score2.get(0), s2n);
    }
    
        /**
     * Test of getSetScore method, of class MatchResult.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetSetScoreNoSet() throws Exception {
        assertThrows(NoSetResultException.class, () -> {mr.getSetScore(0);});
    }

    @Test
    public void testGetSetScoreValue1() throws Exception {
        int s1 = 10, s2 = 20;
        mr.addSetScore(s1, s2);
        assertEquals(mr.getSetScore(0)[0],s1);
    }
    
    @Test
    public void testGetSetScoreValue2() throws Exception {
        int s1 = 10, s2 = 20;
        mr.addSetScore(s1, s2);
        assertEquals(mr.getSetScore(0)[1],s2);
    }

    /**
     * Test of getPlayer1PointsGive method, of class MatchResult.
     */
    @Test
    public void testGetPlayer1PointsGiveNoSets() {
        assertEquals(mr.getPlayer1PointsGive(), 0);
    }
    
    @Test
    public void testGetPlayer1PointsGiveTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(s1, 0);
        mr.addSetScore(s2, 0);
        assertEquals(mr.getPlayer1PointsGive(), s1+s2);
    }

    /**
     * Test of getPlayer2PointsGive method, of class MatchResult.
     */
    @Test
    public void testGetPlayer2PointsGiveNoSets() {
        assertEquals(mr.getPlayer2PointsGive(), 0);
    }
    
    @Test
    public void testGetPlayer2PointsGiveTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(0,s1);
        mr.addSetScore(0,s2);
        assertEquals(mr.getPlayer2PointsGive(), s1+s2);
    }

    /**
     * Test of getPlayer1PointsGot method, of class MatchResult.
     */
    @Test
    public void testGetPlayer1PointsGotNoSets() {
        assertEquals(mr.getPlayer1PointsGot(), 0);
    }
    
    @Test
    public void testGetPlayer1PointsGotTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(0,s1);
        mr.addSetScore(0,s2);
        assertEquals(mr.getPlayer1PointsGot(), s1+s2);
    }

    /**
     * Test of getPlayer2PointsGot method, of class MatchResult.
     */
    @Test
    public void testGetPlayer2PointsGotNoSets() {
        assertEquals(mr.getPlayer2PointsGot(), 0);
    }
    
    @Test
    public void testGetPlayer2PointsGotTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(s1,0);
        mr.addSetScore(s2,0);
        assertEquals(mr.getPlayer2PointsGot(), s1+s2);
    }

    /**
     * Test of getPlayer1SetsGive method, of class MatchResult.
     */
    @Test
    public void testGetPlayer1SetsGiveNoSets() {
        assertEquals(mr.getPlayer1SetsGive(), 0);
    }
    
    @Test
    public void testGetPlayer1SetsGiveTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(s1, 0);
        mr.addSetScore(s2, 0);
        assertEquals(mr.getPlayer1SetsGive(), 2);
    }

    /**
     * Test of getPlayer2SetsGive method, of class MatchResult.
     */
    @Test
    public void testGetPlayer2SetsGiveNoSets() {
        assertEquals(mr.getPlayer2SetsGive(), 0);
    }
    
    @Test
    public void testGetPlayer2SetsGiveTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(0,s1);
        mr.addSetScore(0,s2);
        assertEquals(mr.getPlayer2SetsGive(), 2);
    }

    /**
     * Test of getPlayer1SetsGot method, of class MatchResult.
     */
    @Test
    public void testGetPlayer1SetsGotNoSets() {
        assertEquals(mr.getPlayer1SetsGot(), 0);
    }
    
    @Test
    public void testGetPlayer1SetsGotTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(0,s1);
        mr.addSetScore(0,s2);
        assertEquals(mr.getPlayer1SetsGot(), 2);
    }

    /**
     * Test of getPlayer2SetsGot method, of class MatchResult.
     */
    @Test
    public void testGetPlayer2SetsGotNoSets() {
        assertEquals(mr.getPlayer2SetsGot(), 0);
    }
    
    @Test
    public void testGetPlayer2SetsGotTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(s1,0);
        mr.addSetScore(s2,0);
        assertEquals(mr.getPlayer2SetsGot(), 2);
    }
    
    /**
     * Test of getPlayer1MatchGive method, of class MatchResult.
     */
    @Test
    public void testGetPlayer1MatchGiveNoSets() {
        assertEquals(mr.getPlayer1MatchGive(), 0);
    }
    
    @Test
    public void testGetPlayer1MatchGiveTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(s1, 0);
        mr.addSetScore(s2, 0);
        assertEquals(mr.getPlayer1MatchGive(), 1);
    }

    /**
     * Test of getPlayer2MatchGive method, of class MatchResult.
     */
    @Test
    public void testGetPlayer2MatchGiveNoSets() {
        assertEquals(mr.getPlayer2SetsGive(), 0);
    }
    
    @Test
    public void testGetPlayer2MatchGiveTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(0,s1);
        mr.addSetScore(0,s2);
        assertEquals(mr.getPlayer2MatchGive(), 1);
    }

    /**
     * Test of getPlayer1MatchGot method, of class MatchResult.
     */
    @Test
    public void testGetPlayer1MatchGotNoSets() {
        assertEquals(mr.getPlayer1MatchGot(), 0);
    }
    
    @Test
    public void testGetPlayer1MatchGotTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(0,s1);
        mr.addSetScore(0,s2);
        assertEquals(mr.getPlayer1MatchGot(), 1);
    }

    /**
     * Test of getPlayer2MatchGot method, of class MatchResult.
     */
    @Test
    public void testGetPlayer2MatchGotNoSets() {
        assertEquals(mr.getPlayer2MatchGot(), 0);
    }
    
    @Test
    public void testGetPlayer2MatchGotTwoSets() throws SamePlayerResultException {
        int s1 = 10, s2 = 5;
        mr.addSetScore(s1,0);
        mr.addSetScore(s2,0);
        assertEquals(mr.getPlayer2MatchGot(), 1);
    }
    
    /**
     * Test of SetDiffAndPointsDiffBasedResult method, of class MatchResult.
     */
    @Test
    public void testSetDiffAndPointsDiffBasedResultNoSet() {
        assertEquals(mr.SetDiffAndPointsDiffBasedResult(), 0);
    }
    
    @Test
    public void testSetDiffAndPointsDiffBasedResult() throws SamePlayerResultException {
        mr.addSetScore(0, 0);
        assertEquals(mr.SetDiffAndPointsDiffBasedResult(), 0);
    }


    /**
     * Test of getNumberOfPlayedSet method, of class MatchResult.
     */
    @Test
    public void testGetNumberOfPlayedSetNoSet() {
        assertEquals(mr.getNumberOfPlayedSet(), 0);
    }
    
    @Test
    public void testGetNumberOfPlayedSet() throws SamePlayerResultException {
        mr.addSetScore(0, 0);
        assertEquals(mr.getNumberOfPlayedSet(), 1);
    }
        
}
