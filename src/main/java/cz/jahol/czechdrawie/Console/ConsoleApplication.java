/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Console;

import cz.jahol.czechdrawie.Core.DrawTable;
import cz.jahol.czechdrawie.Exceptions.PlayerNotExistException;
import cz.jahol.czechdrawie.Exceptions.SamePlayerResultException;
import cz.jahol.czechdrawie.Exceptions.SetNotAddYetException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author jahol
 */
public class ConsoleApplication {    
    
    public static void main(String[] args) {
        // Using Scanner for Getting Input from User 
        Scanner in = new Scanner(System.in); 
        DrawTable tournament = new DrawTable();        
        String[] s;
        
        
        String help = "Operation:\n"
                + "help\n"
                + "SName :TournamentName\n"
                + "AP :NameOfPlayerToAdd\n"
                + "RP :NameOfPlayerToRemove\n"
                + "AS :NamePlayer1 NamePlayer2 score1 score2\n"
                + "CS :NamePlayer1 NamePlayer2 score1 score2 setNumber\n"
                + "Points :NamePlayer\n"
                + "Result\n";
        
        System.out.println(help);
  
        boolean run = true;
        while(run){
            System.out.println("Type command:");
            try{
            s = in.nextLine().split(":", 2);
            switch(s[0]){
                case "End":
                    run = false;
                    break;
                case "Sname":
                    tournament.setName(s[1]);
                    break;
                case "AP":
                    tournament.addPlayer(s[1]);
                    break;
                case "RP":
                    tournament.removePlayer(s[1]);
                    break;
                case "AS":
                    s = s[1].split(" ", 4);
                    tournament.addSetResult(s[0], s[1], Integer.parseInt(s[2]), Integer.parseInt(s[3]));
                    break;
                case "CS":
                    s = s[1].split(" ", 5);
                    tournament.changeSetResult(s[0], s[1], Integer.parseInt(s[2]), Integer.parseInt(s[3]), Integer.parseInt(s[4])+1);
                    break;
                case "Points":
                    System.out.println("Player: " + s[1] +
                            "Matches: " + tournament.computePlayerResultMatchesGive(s[1]) + " | " + tournament.computePlayerResultMatchesGot(s[1]) + "\n" +
                            "Sets: " + tournament.computePlayerResultSetsGive(s[1]) + " | " + tournament.computePlayerResultSetsGot(s[1]) + "\n" +
                            "Points: " + tournament.computePlayerResultPointsGive(s[1]) + " | " + tournament.computePlayerResultPointsGot(s[1])
                            );
                    break;
                case "Result":
                    ArrayList<Integer> resultIndex = tournament.getPlayersOrder_MatchesSetsPointsDiff();
                    String report = tournament.getName() + " result report:\n";
                    for (int i = 0; i < resultIndex.size(); i++) {
                        report += (i+1) + ". " + tournament.getPlayers().get(i) + "\n";
                    }
                    System.out.println(report);
                    break;
                case "help":
                    System.out.println(help);
                    break;
                default:
                    System.out.println("Unknown operation: " + s[0]);
                 
            }    
            }catch (PlayerNotExistException e){
                System.out.println(e);
            }catch (SamePlayerResultException e){
                System.out.println(e);
            }catch (SetNotAddYetException e){
                System.out.println(e);
            }
                  
        }
         
    }
    
}
