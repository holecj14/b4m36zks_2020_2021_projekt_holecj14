/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Exceptions;

import cz.jahol.czechdrawie.Core.Player;

/**
 *
 * @author jahol
 */
public class PlayerNotExistException extends Exception {
    
    private final Player player;
    
    public PlayerNotExistException(Player player) {
        this.player = player; 
    }

    
    @Override
    public String toString() {
        return "Player " + player + " do not exist."; 
    }
}
