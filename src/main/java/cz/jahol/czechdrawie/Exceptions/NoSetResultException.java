/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Exceptions;

import cz.jahol.czechdrawie.Core.Player;

/**
 *
 * @author jahol
 */
public class NoSetResultException extends Exception {

    private int set;
    private Player player1;
    private Player player2;
    
    public NoSetResultException(Player player1, Player player2, int set) {
        this.player1 = player1;
        this.player2 = player2;
        this.set = set;    
    }

    
    @Override
    public String toString() {
        return "No " + set + ". set result for players: " + player1.getName() + ", " + player2.getName(); 
    }
    
    
    
}
