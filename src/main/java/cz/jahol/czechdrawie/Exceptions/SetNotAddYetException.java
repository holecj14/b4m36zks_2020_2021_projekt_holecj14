/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Exceptions;

import cz.jahol.czechdrawie.Core.MatchResult;

/**
 *
 * @author jahol
 */
public class SetNotAddYetException extends Exception {
    
    private final int setNumber;
    private final MatchResult matchResult;

    public SetNotAddYetException(int setNumber, MatchResult matchResult) {
        this.setNumber = setNumber;
        this.matchResult = matchResult;
    }
    
    
    @Override
    public String toString() {
        return "In MatchResult of players " + matchResult.getPlayer1().getName() + " | " + matchResult.getPlayer2().getName() + " was not set number " + setNumber + "played yet - number of set played: " + matchResult.getNumberOfPlayedSet(); 
    }
    
    
}
