/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Core;

import cz.jahol.czechdrawie.Exceptions.PlayerNotExistException;
import cz.jahol.czechdrawie.Exceptions.SamePlayerResultException;
import cz.jahol.czechdrawie.Exceptions.SetNotAddYetException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author jahol
 */
public class DrawTable {
    
    private String name;
    private ArrayList<Player> players;
    ArrayList<ArrayList<MatchResult>> matchesResults; /*2D array of matchResults*/

    public DrawTable() {
        this.players = new ArrayList<>();
        this.matchesResults = new ArrayList<>();
        name = "";
    }
    
    public void addPlayer(Player player){
        players.add(player);
        this.matchesResults.add(new ArrayList<>());
        
        matchesResults.forEach(next -> {
            next.ensureCapacity(players.size());
        });
        
        /*For last (added) player prepare all MatchResult*/
        for (int i = 0; i < players.size(); i++) {
            matchesResults.get(players.size()-1).add(new MatchResult(player, players.get(i)));
        }
        
        /*For all old players add Match with new player*/
        for (int i = 0; i < players.size()-1; i++) {
            matchesResults.get(i).add(new MatchResult(players.get(i), player));
        }
    }
    
    public void addPlayer(String playerName){
        this.addPlayer(new Player(playerName));
    } 
     
    public void removePlayer(Player player) throws PlayerNotExistException{
        int playerIndex = players.indexOf(player);
        if(playerIndex == -1){
            throw new PlayerNotExistException(player);
        }
        players.remove(playerIndex);
        matchesResults.remove(playerIndex);
        
        /*Remove matches from other players */
        for (Iterator<ArrayList<MatchResult>> iterator = matchesResults.iterator(); iterator.hasNext();) {
            iterator.next().remove(playerIndex);            
        }
    }
    
    public void removePlayer(String player) throws PlayerNotExistException{
        removePlayer(new Player(player));
    }
    
    public int getPlayerIndex(String player){
        return players.indexOf(new Player(player));
    }
        
    public void addSetResult(String player1, String player2, int resultPlayer1, int resultPlayer2) throws SamePlayerResultException{
        int player1id = getPlayerIndex(player1);
        int player2id = getPlayerIndex(player2);
        matchesResults.get(player1id).get(player2id).addSetScore(resultPlayer1, resultPlayer2);
        matchesResults.get(player2id).get(player1id).addSetScore(resultPlayer2, resultPlayer1);
    }
    
    public void changeSetResult(int setNumber, int player1id, int player2id, int resultPlayer1, int resultPlayer2) throws SamePlayerResultException, SetNotAddYetException{
        matchesResults.get(player1id).get(player2id).changeSetScore(setNumber, resultPlayer1, resultPlayer2);
        matchesResults.get(player2id).get(player1id).changeSetScore(setNumber, resultPlayer2, resultPlayer1);
    }
   
    public void changeSetResult(String player1, String player2, int resultPlayer1, int resultPlayer2, int setNumber) throws SamePlayerResultException, SetNotAddYetException{
        int player1id = getPlayerIndex(player1);
        int player2id = getPlayerIndex(player2);
        changeSetResult(setNumber, player1id, player2id, resultPlayer1, resultPlayer2);
    }
      
    public int computePlayerResultPointsGive(String player){
        int playerIndex = getPlayerIndex(player);
        int playerPoints = 0;
        for (Iterator<MatchResult> iterator = matchesResults.get(playerIndex).iterator(); iterator.hasNext();) {
            playerPoints += iterator.next().getPlayer1PointsGive();            
        }
        return playerPoints;
    }
    
    public int computePlayerResultPointsGot(String player){
        int playerIndex = getPlayerIndex(player);
        int playerPoints = 0;
        for (Iterator<MatchResult> iterator = matchesResults.get(playerIndex).iterator(); iterator.hasNext();) {
            playerPoints += iterator.next().getPlayer1PointsGot();            
        }
        return playerPoints;
    }
    
    public int computePlayerResultPointsDiff(String player){
        return computePlayerResultPointsGive(player) - computePlayerResultPointsGot(player);
    }
    
    public int computePlayerResultSetsGive(String player){
        int playerIndex = getPlayerIndex(player);
        int playerSets = 0;
        for (Iterator<MatchResult> iterator = matchesResults.get(playerIndex).iterator(); iterator.hasNext();) {
            playerSets += iterator.next().getPlayer1SetsGive();            
        }
        return playerSets;
    }
    
    public int computePlayerResultSetsGot(String player){
        int playerIndex = getPlayerIndex(player);
        int playerSets = 0;
        for (Iterator<MatchResult> iterator = matchesResults.get(playerIndex).iterator(); iterator.hasNext();) {
            playerSets += iterator.next().getPlayer1SetsGot();            
        }
        return playerSets;
    }
    
    public int computePlayerResultSetsDiff(String player){
        return computePlayerResultSetsGive(player) - computePlayerResultSetsGot(player);
    }
    
    public int computePlayerResultMatchesGive(String player){
        int playerIndex = getPlayerIndex(player);
        int playerMatches = 0;
        for (Iterator<MatchResult> iterator = matchesResults.get(playerIndex).iterator(); iterator.hasNext();) {
            if (iterator.next().getPlayer1MatchGive() > 0){
                playerMatches ++;            
            }
        }
        return playerMatches;
    }
    
    public int computePlayerResultMatchesGot(String player){
        int playerIndex = getPlayerIndex(player);
        int playerMatches = 0;
        for (Iterator<MatchResult> iterator = matchesResults.get(playerIndex).iterator(); iterator.hasNext();) {
            if (iterator.next().getPlayer1MatchGot() > 0){
                playerMatches ++;            
            }
        }
        return playerMatches;
    }
    
    public int computePlayerResultMatchesDiff(String player){
        return computePlayerResultMatchesGive(player) - computePlayerResultMatchesGot(player);
    }
    
    public ArrayList<Integer> getPlayersOrder_MatchesSetsPointsDiff(){
        ArrayList<Integer> matches = new ArrayList<>(players.size());
        ArrayList<Integer> sets = new ArrayList<>(players.size());
        ArrayList<Integer> points = new ArrayList<>(players.size());
        
        for (Player player : players) {
            matches.add(computePlayerResultMatchesDiff(player.getName()));
            sets.add(computePlayerResultSetsDiff(player.getName()));
            points.add(computePlayerResultPointsDiff(player.getName()));
        }
        
        MatchesSetsPointsComparator comparator = new MatchesSetsPointsComparator(matches, sets, points);
        ArrayList<Integer> indexes = comparator.createIndexArray();
        indexes.sort(comparator);
        return indexes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }
    
    
}
