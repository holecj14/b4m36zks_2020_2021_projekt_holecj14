/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Core;

import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author jahol
 */
public class MatchesSetsPointsComparator implements Comparator<Integer>
{
    private final ArrayList<Integer> matches;
    private final ArrayList<Integer> sets;
    private final ArrayList<Integer> points;

    public MatchesSetsPointsComparator(ArrayList<Integer> matches, ArrayList<Integer> sets, ArrayList<Integer> points)
    {
        this.matches = matches;
        this.sets = sets;
        this.points = points;
    }

    public ArrayList<Integer> createIndexArray()
    {
        ArrayList<Integer> indexes = new ArrayList<>(matches.size());
        for (int i = 0; i < matches.size(); i++) {
            indexes.add(i);            
        }
        return indexes;
    }

    @Override
    public int compare(Integer index1, Integer index2)
    {
        int matchComp = Integer.compare(matches.get(index1), matches.get(index2));
        int setComp = Integer.compare(sets.get(index1), sets.get(index2));
        int pointComp = Integer.compare(points.get(index1), points.get(index2));
        
        if(matchComp == 0){
            if(setComp == 0){
                return pointComp;
            }else{
                return setComp;
            }
        }else{
            return matchComp;
        }
        
    }
}