/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jahol.czechdrawie.Core;

import cz.jahol.czechdrawie.Exceptions.SetNotAddYetException;
import cz.jahol.czechdrawie.Exceptions.NoSetResultException;
import cz.jahol.czechdrawie.Exceptions.SamePlayerResultException;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author jahol
 */
public class MatchResult {
    
    Player player1;
    Player player2;
    final ArrayList<Integer> score1;
    final ArrayList<Integer> score2;

    public MatchResult(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        score1 = new ArrayList<>();
        score2 = new ArrayList<>();
    }

    public Player getPlayer2() {
        return player2;
    }

    public Player getPlayer1() {
        return player1;
    }
 
    public int getNumberOfPlayedSet(){
        return score1.size();
    }
    
    public void addSetScore(int score1, int score2) throws SamePlayerResultException{ /*For simplicity there can be MatchResult object, but can not result itself*/
        if (player1.equals(player2)){
            throw new SamePlayerResultException(player1);
        }
        this.score1.add(score1);
        this.score2.add(score2);
    }
    
    public void changeSetScore(int setNumber, int score1, int score2) throws SamePlayerResultException, SetNotAddYetException{ /*For simplicity there can be MatchResult object, but can not result itself*/
        if (player1.equals(player2)){
            throw new SamePlayerResultException(player1);
        }
        if( this.score1.size() <= setNumber){
            throw new SetNotAddYetException(setNumber, this);
        }
        this.score1.set(setNumber, score1);
        this.score2.set(setNumber, score2);
    }

    public int[] getSetScore(int set) throws NoSetResultException{
        try {
            int score[] = {score1.get(set), score2.get(set)};
            return score;
        } catch (IndexOutOfBoundsException e) {
            throw new NoSetResultException(player1, player2, set);
        }       
    }
    
    public int getPlayer1PointsGive(){
        int sum = 0;
        for(Integer set : score1)
            sum += set;
        return sum;
    }
    
    public int getPlayer2PointsGive(){
        int sum = 0;
        for(Integer set : score2)
            sum += set;
        return sum;
    }
    
    public int getPlayer1PointsGot(){
        return getPlayer2PointsGive();
    }
    
    public int getPlayer2PointsGot(){
        return getPlayer1PointsGive();
    }
    
    public int getPlayer1SetsGive(){
        int sum = 0;
        for (int i = 0; i < score1.size(); i++) {
            if(score1.get(i) > score2.get(i)){
                sum++;
            }
        }
        return sum;
    }
    
    public int getPlayer2SetsGive(){
        int sum = 0;
        for (int i = 0; i < score1.size(); i++) {
            if(score1.get(i) < score2.get(i)){
                sum++;
            }
        }
        return sum;
    }

    public int getPlayer1SetsGot(){
        return getPlayer2SetsGive();
    }
    
    public int getPlayer2SetsGot(){
        return getPlayer1SetsGive();
    }
    
    public int SetDiffAndPointsDiffBasedResult(){
        
        int setDiff = getPlayer1SetsGive() - getPlayer2SetsGive();
        int pointsDiff = getPlayer1PointsGive() - getPlayer2PointsGive();
        if (setDiff > 0){
            return 1;
        }else if (setDiff < 0){
            return -1;
        }else{
            if (pointsDiff > 0){
                return 1;
            }else if (pointsDiff < 0){
                return -1;
            }else {return 0;}
        } 
    }
    
    public int getPlayer1MatchGive(){
        return SetDiffAndPointsDiffBasedResult();               
    }
    
    public int getPlayer2MatchGive(){  
        return -SetDiffAndPointsDiffBasedResult();
    }
    
    public int getPlayer1MatchGot(){
        return -getPlayer1MatchGive();
    }
    
    public int getPlayer2MatchGot(){
        return -getPlayer2MatchGive();
    }

    @Override
    public String toString() {
        String buff = "";
        buff += "MatchResult: " + player1 + " - | ";
        for (int i = 0; i < score1.size(); i++) {
            buff += score1.get(i) + " : " + score2.get(i) + " | ";
        }
        buff += player2;
        return buff;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.player1);
        hash = 41 * hash + Objects.hashCode(this.player2);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MatchResult other = (MatchResult) obj;
        if (!Objects.equals(this.player1, other.player1)) {
            return false;
        }
        if (!Objects.equals(this.player2, other.player2)) {
            return false;
        }
        return true;
    }
    
    
    
}
